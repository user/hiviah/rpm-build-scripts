#!/bin/bash

#####
#
# Script creates directory structure with RPM metadata in suitable form for yum.
#
# arg1 - source directory with built RPM files
# arg2 - target directory where repodata and dir structure with RPMs should be created
#

if [ -z "$2" ]; then
    echo "$0 source_dir dest_dir"
    exit 1
fi

SRC="$1"
DEST="$2"
REPOMD_SIGNING_KEY_ID="0xF4B85E0F"

mkdir -p "$DEST"

for I in "$SRC"/tor-*.rpm; do
    OS=$(echo "$I"| sed 's/^.*\.\([^\.]*\)\.[^\.]*\.rpm$/\1/')
    ARCH=$(echo "$I"| sed 's/^.*[^\.]*\.\([^\.]*\)\.rpm$/\1/')

    case "$ARCH" in
        src) ARCH_DIR=SRPMS
            ;;
        *) ARCH_DIR="$ARCH"
            ;;
    esac

    case "$OS" in
        rh16) OS_DIR=fc/16
            ;;
        rh17) OS_DIR=fc/17
            ;;
        rh18) OS_DIR=fc/18
            ;;
        rh19) OS_DIR=fc/19
            ;;
        rh20) OS_DIR=fc/20
            ;;
        rh21) OS_DIR=fc/21
            ;;
        rh22) OS_DIR=fc/22
            ;;
        rh23) OS_DIR=fc/23
            ;;
        rh5_*) OS_DIR=el/5
            ;;
        rh6_*) OS_DIR=el/6
            ;;
        rh7_*) OS_DIR=el/7
            ;;
        *) echo "Unknown OS/distro"; exit 2
    esac
    
    DESTDIR="$DEST/$OS_DIR/$ARCH_DIR"
    mkdir -p "$DESTDIR"
    echo Processing $I == OS: $OS, ARCH: $ARCH, DIR: $DESTDIR
    cp -a "$I" "$DESTDIR"

    if [ "$ARCH_DIR" = "i686" ]; then
        #$basearch in yum.repo file will be i386, so symlink i386->i686
        LINKNAME="$DESTDIR/../i386"
        if [ '!' -e "$LINKNAME" ]; then
            ln -s i686 "$LINKNAME"
        fi
    fi
done

for REPO in "$DEST"/*/*/{i386,i686,x86_64,SRPMS}; do 
    #EL5 needs to get older hash for checksums specified
    CREATEREPO_ARGS=""
    if [ $(echo "$REPO" | grep -c "el/5") -gt 0 ]; then 
        echo " - Using sha checksum for repo $REPO"
        CREATEREPO_ARGS="-s sha"
    fi

    echo "Metadata for $REPO"
    createrepo $CREATEREPO_ARGS "$REPO"

done

# Sign each repomd.xml
for I in $(find "$DEST" -name "repomd.xml"); do gpg --detach-sign --armor --local-user $REPOMD_SIGNING_KEY_ID "$I"; done

